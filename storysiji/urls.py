from django.urls import path
from storysiji import views

urlpatterns = [
    path('', views.storysiji),
]
