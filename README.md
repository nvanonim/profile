# More Like Profile

2019/2020 - Story 4 for PPW

## Introduction

- Name  : Alghifari Taufan Nugroho
- NPM   : 1806146846
- URL   : [Alghifari](https://alghifari-tn.herokuapp.com/)

## Major Commit Changelog

1.  Initial commit
    - Added Home, About, Gallery, Contact in [Alghifari/](https://alghifari-tn.herokuapp.com/)

2.  add project page
    - Added Project page in [Alghifari/project/](https://alghifari-tn.herokuapp.com/project)

3.  add storysiji
    - Added Story1 app in [Alghifari/siji/](https://alghifari-tn.herokuapp.com/siji)
    - Added Error404 page *Check [Alghifari/404/](https://alghifari-tn.herokuapp.com/404) when loaded locally*
    - Minor fix on font

4.  Add Responsive Stage 1 | **Story 3 - Sept 24th 2019**
    - Added support for phone user on Home, Gallery, Contact, and Project pages

5.  Add Responsive Stage 2
    - Added support for phone user on About page
    - Minor changes on About page
    - Minor changes on Error404 page

6.  Add templates | **Story 4 - Sept 27th 2019**
    - Modified all page with templates

7.  Add schedule html
    - Added 1 model
    - Added forms for the model
    - Added schedule form and schedule output in [Alghifari/schedule/](https://alghifari-tn.herokuapp.com/schedule)

8.  Add bootstrap forms
    - Changed the schedule form design

9. Add delete function
    - Added delete function
    - Cleaning up the schedule output