from django.db import models

# Create your models here.
class Sched(models.Model):
    name = models.CharField(max_length = 50)
    place = models.CharField(max_length = 50)
    category = models.CharField(max_length = 50)
    date = models.DateField()
    time = models.TimeField()
