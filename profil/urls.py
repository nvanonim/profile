from django.urls import path
from profil import views

urlpatterns = [
    path('', views.index, name='index'),
    path('project/', views.project, name='project'),
    path('404/', views.error404),
    path('schedule/', views.sched, name='schedule'),
]
