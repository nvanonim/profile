from django.shortcuts import render, redirect
from profil import forms, models
from django.http import HttpResponseRedirect
from datetime import datetime
from .models import Sched


# Create your views here.
def index(request):
    return render(request, 'index.html', {})
def project(request):
    return render(request, 'project.html', {})
def error404(request, exception=None):
    return render(request, '404.html')
def sched(request):
    if request.method == "POST":
        form = forms.SchedForm(request.POST)
        if 'id' in request.POST:
            Sched.objects.get(id=request.POST['id']).delete()
            return redirect('/schedule/')
        if form.is_valid():
            sched = models.Sched(
                name = form.data['name'],
                place = form.data['place'],
                category = form.data['category'],
                date = form.data['date_year'] + "-" + form.data['date_month'] + "-" + form.data['date_day'],
                time = form.data['time'],
                )
            sched.save()
            return redirect('/schedule/')

    else:
        form = forms.SchedForm()
        

    schedule_context = {
        'schedules_active' : 'active',
        'context' : {'now': datetime.now()},
        'schedules': models.Sched.objects.all().values(),
        'form' : form,
        }
    return render(request, 'schedule.html', schedule_context)