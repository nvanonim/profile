from django import forms
from profil import models

years = [x for x in range(2019, 2025)]

class SchedForm(forms.Form):
    name = forms.CharField(
        label = 'Event Name',
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "PPW"}),
    )
    place = forms.CharField(
        label = 'Place',
        max_length = 50,
        required = True,
        widget = forms.TextInput(attrs = {'class' : 'form-control form-control-sm', 'placeholder' : "Fasilkom UI"}),
    )
    category = forms.ChoiceField(
        label = 'Category',
        choices=[('personal', 'Personal'),
                ('College', 'College')],
        widget = forms.Select(attrs = {'class' : 'form-control form-control-sm'})
    )
    date = forms.DateField(
        label = 'Date',
        required = True,
        widget = forms.SelectDateWidget(years = years, attrs = {'class' : 'form-control-sm', 'style' : 'border: 1px solid #ced4da; background: white; width: 33.3%;'}),
    )
    time = forms.TimeField(
        label = 'Time',
        input_formats = ['%H:%M'],
        widget = forms.TextInput(attrs={'class' : 'form-control form-control-sm', 'placeholder' : '00:00'}),
    )
    
    class Meta:
        model = models.Sched
        fields = ('name', 'place', 'category', 'date', 'time',)
